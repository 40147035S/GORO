#ifndef THE_BOARD_H_INCLUDED
#define THE_BOARD_H_INCLUDED
//using the most simple implementation

#include <cstdio>

class game {    //tic-tac-toe
public:
    void new_game();    //0 for nothing, 1 for O, -1 for X
    bool game_over();
private:
    const static size_t LEN = 3;
    int myboard[LEN][LEN];    //we use 3 X 3 for a tic-tac-toe
};



#endif // THE_BOARD_H_INCLUDED
